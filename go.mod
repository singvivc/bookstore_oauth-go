module gitlab.com/singvivc/bookstore_oauth-go

go 1.14

require (
	github.com/stretchr/testify v1.5.1
	gitlab.com/singvivc/bookstore_utils-go v0.0.0-20200420191921-591738e9b2e5
)
