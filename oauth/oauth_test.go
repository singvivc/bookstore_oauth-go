package oauth

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	fmt.Println("About to start oauth test")
	os.Exit(m.Run())
}

func TestOauthConstants(t *testing.T) {
	assert.EqualValues(t, "X-Public", headerXPublic)
	assert.EqualValues(t, "X-Client-Id", headerXClientId)
	assert.EqualValues(t, "X-Caller-Id", headerXCallerId)
	assert.EqualValues(t, "access_token", paramAccessToken)
}

func TestIsPublicWithNilRequest(t *testing.T) {
	assert.True(t, IsPublic(nil))
}

func TestIsPublicWithNoError(t *testing.T) {
	request := http.Request{Header: make(http.Header)}
	assert.False(t, IsPublic(&request))

	request.Header.Add(headerXPublic, "true")
	assert.True(t, IsPublic(&request))
}

func TestGetCallerIdWithNilRequest(t *testing.T) {

}

func TestGetCallerIdWithIncorrectHeader(t *testing.T) {

}

func TestGetCallerIdWithNoError(t *testing.T) {

}

func TestGetClientIdWithNilRequest(t *testing.T) {

}

func TestGetClientIdWithIncorrectHeader(t *testing.T) {

}

func TestGetClientIdWithNoError(t *testing.T) {

}

func TestAuthenticate(t *testing.T) {

}

func TestGetAccessTokenInvalidRestClientResponse(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusInternalServerError)
	}))
	defer testServer.Close()

	accessToken, err := getAccessToken("abctdwh")
	assert.Nil(t, accessToken)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status())
	assert.EqualValues(t, "Invalid restClient response while getting the access token", err.Message())
}
