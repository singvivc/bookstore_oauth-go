package oauth

import (
	"encoding/json"
	"fmt"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)
var baseUrl = "http://localhost:8080"
//var restClient = rest.RequestBuilder{BaseURL: "http://localhost:8080", Timeout: 200 * time.Millisecond}

const (
	headerXPublic   = "X-Public"
	headerXClientId = "X-Client-Id"
	headerXCallerId = "X-Caller-Id"

	paramAccessToken = "access_token"
)

type accessToken struct {
	Id       string `json:"id"`
	UserId   int64  `json:"user_id"`
	ClientId int64  `json:"client_id"`
}

func IsPublic(request *http.Request) bool {
	if request == nil {
		return true
	}
	return request.Header.Get(headerXPublic) == "true"
}

func GetCallerId(request *http.Request) int64 {
	if request == nil {
		return 0
	}
	callerId, err := strconv.ParseInt(request.Header.Get(headerXCallerId), 10, 64)
	if err != nil {
		return 0
	}
	return callerId
}

func GetClientId(request *http.Request) int64 {
	if request == nil {
		return 0
	}
	clientId, err := strconv.ParseInt(request.Header.Get(headerXClientId), 10, 64)
	if err != nil {
		return 0
	}
	return clientId
}

// Authenticate will authenticate the current request
func Authenticate(request *http.Request) errors.RestError {
	if request == nil {
		return nil
	}
	cleanRequest(request)
	accessTokenId := strings.TrimSpace(request.URL.Query().Get(paramAccessToken))
	if accessTokenId == "" {
		return nil
	}
	accessToken, err := getAccessToken(accessTokenId)
	if err != nil {
		if err.Status() == http.StatusNotFound {
			return nil
		}
		return err
	}
	request.Header.Add(headerXClientId, fmt.Sprintf("%v", accessToken.ClientId))
	request.Header.Add(headerXCallerId, fmt.Sprintf("%v", accessToken.UserId))
	return nil
}

func cleanRequest(request *http.Request) {
	if request == nil {
		return
	}
	request.Header.Del(headerXClientId)
	request.Header.Del(headerXCallerId)
}

func getAccessToken(accessTokenId string) (*accessToken, errors.RestError) {
	response, err := http.Get(fmt.Sprintf(baseUrl + "/oauth/token/%s", accessTokenId))
	if err != nil {
		return nil, errors.InternalServerError("Invalid restClient response while getting the access token")
	}

	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, errors.InternalServerError("error occurred while unmarshalling the access token response")
	}
	var accessToken accessToken
	if err := json.Unmarshal(bytes, &accessToken); err != nil {
		return nil, errors.InternalServerError("error occurred while unmarshalling the access token response")
	}
	return &accessToken, nil
}
